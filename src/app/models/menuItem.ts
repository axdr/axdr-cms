export interface menuItem 
{
    _title: string;
    _path?: string;
    _children?: menuItem[];
}