export const globalRoutesNames = {
    DEFAULT: 
    {
      url: '',
      title: 'undefined'
    },
    LOGIN: 
    {
      url: '',
      title: 'Login'
    },
    REGISTER: 
    {
      url: 'quickregister',
      title: 'Registro'
    },
    ME: 
    {
      url: 'me',
      title: 'Home',
    },
    SETTINGS: 
    {
      url: 'settings',
      title: 'Ajustes',
    },
    GENERAL: 
    {
      url: '',
      title: 'General',
    },
    PREFERENCES: 
    {
      url: 'preferences',
      title: 'Preferencias',
    },
    CUSTOMIZATION: 
    {
      url: 'customization',
      title: 'Personalización',
    },
    PROFILE: {
      url: 'profile',
      title: 'Mi Perfil'
    },
    COMMUNITY: {
      url: 'community',
      title: 'Comunidad'
    },
    ARTICLES: {
      url: 'articles',
      title: 'Noticias'
    },
    PHOTO_GALLERY: {
      url: 'photo-gallery',
      title: 'Galería de Fotos'
    },
    TOP: {
      url: 'top',
      title: 'Tops'
    },
    TEAM: {
      url: 'team',
      title: 'Equipo'
    },
    DISCOVER: {
      url: 'playing-habbo',
      title: 'Descubre Habbo'
    },
    WHATIS: {
      url: 'what-is-habbo',
      title: '¿Qué es Habbo?'
    },
    HOWTOPLAY: {
      url: 'how-to-play',
      title: '¿Cómo Jugar?'
    },
    HELP: {
      url: 'help',
      title: 'Ayuda'
    }
  }